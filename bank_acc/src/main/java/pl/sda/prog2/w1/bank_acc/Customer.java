package pl.sda.prog2.w1.bank_acc;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class Customer {
    private String imie;
    private String nazwisko;
    private String pesel;
    private int id;
    List<Account> cust_acc_list;

    public Customer(String imie, String nazwisko, String pesel, int id) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.pesel = pesel;
        this.id = id;
        this.cust_acc_list = new ArrayList<Account>();
    }

    public void add_account(Account konto) {
        this.cust_acc_list.add(konto);
    }

    public void close_account(Account konto) {
        this.cust_acc_list.remove(konto);
    }

    public boolean noAcc() {
        if (this.cust_acc_list.stream().count() == 0) {
            return true;
        } return false;
    }

    public String description() {
        return "imie='" + imie +
                ", nazwisko='" + nazwisko +
                ", pesel='" + pesel +
                ", id=" + id + ", cust_acc_list=" +
                cust_acc_list.stream()
                        .map(account -> account.description())
                        .collect(Collectors.toList()) + ", suma wszystkich sald=" + this.getSaldo()
                ;

    }

    public String descriptionShort() {
        return "imie='" + imie +
                ", nazwisko='" + nazwisko +
                ", pesel='" + pesel +
                ", id=" + id + ", suma wszystkich sald=" + this.getSaldo()
                ;

    }

    public Double getSaldo() {
        return cust_acc_list.stream()
                .mapToDouble(account -> account.getSaldo())
                .sum();
    }

    public boolean oper(typy_kont tk, Double amount) {
        Account ac = cust_acc_list.stream()
                .filter(a -> Objects.equals(a.getTyp(), tk))
                .findFirst()
                .get();
        if (ac.getSaldo()<amount && amount<0) return false;
        ac.setSaldo(amount+ac.getSaldo());

        return true;
    }
}
