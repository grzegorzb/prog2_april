package pl.sda.prog2.w1.bank_acc;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class Bank {
    List<Account> accounts_ls;
    List<Customer> customers_ls;

    public Bank() {
        this.accounts_ls = new ArrayList<Account>();
        this.customers_ls = new ArrayList<Customer>();
    }

    protected void addAcc(Customer k, typy_kont tk, Double a) {
        Account ac = new Account(tk, a);
        this.accounts_ls.add(ac);
        k.add_account(ac);
    }

    protected void delAcc(Customer k, typy_kont tk) {
        Optional<Account> ac = k.cust_acc_list.stream()
                .filter(a -> Objects.equals(a.getTyp(), tk))
                .findFirst();
        if (ac.get().getSaldo() == 0.0) {
            this.accounts_ls.remove(ac.get());
            k.close_account(ac.get());
        }
    }

    protected Customer addCust(String imie, String nazwisko, String pesel, int id) {
        Customer kl = new Customer(imie, nazwisko, pesel, id);
        this.customers_ls.add(kl);
        return kl;
    }

    protected void delCust(Customer k) {
        if (k.noAcc()) this.customers_ls.remove(k);
    }

    public void showCustomers() {
        System.out.println("Wszyscy klienci banku:");
        this.customers_ls.stream()
                .map(customer -> customer.descriptionShort())
                .forEach(System.out::println)
        ;

    }

    public static void main(String[] args) {
        System.out.println("Start");
        Bank bank = new Bank();
// założenie klienta
        Customer kl1 = bank.addCust("Grzegorz", "Bury", "80010123456", 2);
        Customer kl2 = bank.addCust("Roman", "Akwen", "89020223456", 4);
        Customer kl3 = bank.addCust("Cezary", "Ryn", "78030323456", 6);

// założenie rachunku dla klienta
        bank.addAcc(kl1, typy_kont.ROR_PLN, 1000.00);
        bank.addAcc(kl1, typy_kont.LOK_PLN, 5000.00);
        bank.addAcc(kl2, typy_kont.LOK_PLN, 2000.00);
        bank.addAcc(kl2, typy_kont.ROR_PLN, 900.00);
        bank.addAcc(kl3, typy_kont.ROR_PLN, 1800.00);
        bank.addAcc(kl3, typy_kont.LOK_PLN, 0.00);

//usunięcie rachunku dla klienta (jeśli stan środków = 0)
        System.out.println(kl3.description());
        bank.delAcc(kl3, typy_kont.ROR_PLN);
        bank.delAcc(kl3, typy_kont.LOK_PLN);
        System.out.println("Po usunieciu: "+kl3.description());

//lista klientów banku (z listą rachunków(z saldem lub bez))
        bank.showCustomers();

//wpłata na rachunek
        kl1.oper(typy_kont.LOK_PLN,100.0);
        kl3.oper(typy_kont.ROR_PLN,-50.0);
//wypłata z rachunku (do wysokości salda)

//lista rachunków klienta (z saldem lub bez)
        System.out.println("Poszczegolni klienci z opisem kont i saldem:");
        System.out.println(kl1.description());
        System.out.println(kl2.description());
        System.out.println(kl3.description());

//usunięcie klienta (jeśli nie ma rachunków)
        kl3.oper(typy_kont.ROR_PLN,-1750.0);
        bank.delAcc(kl3, typy_kont.ROR_PLN);
        System.out.println("Usuwam klienta: "+kl3.description());
        bank.delCust(kl3);
        bank.showCustomers();
    }
}
