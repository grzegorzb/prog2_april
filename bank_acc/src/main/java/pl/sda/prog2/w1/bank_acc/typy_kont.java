package pl.sda.prog2.w1.bank_acc;

public enum typy_kont {
    ROR_PLN ("Zawsze Pelny PLN"),
    ROR_EURO ("Always Full EUR"),
    ROR_DOLAR ("Always Full $"),
    LOK_PLN ("Stabilne Jutro PLN"),
    LOK_EURO ("Stable Tomorrow EUR"),
    LOK_DOLAR ("Stable Tomorrow $");

    private final String nazwa_maketingowa;

    typy_kont(String s) {
        this.nazwa_maketingowa=s;
    }

    public String getNazwa_maketingowa() {
        return nazwa_maketingowa;
    }
}
