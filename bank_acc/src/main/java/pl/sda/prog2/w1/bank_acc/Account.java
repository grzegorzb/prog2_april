package pl.sda.prog2.w1.bank_acc;

public class Account {
    public static Account DEFAULT_CREATION; //puste konto
    private typy_kont typ;
    private Double saldo;


    public Account(typy_kont typ, Double saldo) {
        this.typ = typ;
        this.saldo = saldo;
    }

    public String description() {
        return "Konto(" +
                "typ=" + typ.getNazwa_maketingowa() +
                ", saldo=" + saldo +
                ')';
    }

    public Double getSaldo() {
        return saldo;
    }

    public typy_kont getTyp() {
        return typ;
    }

    public Account setSaldo(Double saldo) {
        this.saldo = saldo;
        return this;
    }

}
