package pl.sda.prog2.w1.movie_search;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Runner {
    public static void main(String[] args) {
        Movie movie1 = new Movie("Testosteron", "Tomasz Konecki, Andrzej Saramonowicz", LocalDate.parse("2007-03-02"));
        Movie movie2 = new Movie("Lejdis", "Tomasz Konecki, Andrzej Saramonowicz", LocalDate.parse("2008-02-01"));
        Movie movie3 = new Movie("Chłopaki nie płaczą", "Olaf Lubaszenko", LocalDate.parse("2000-02-25"));
        Movie movie4 = new Movie("Poranek kojota", "Olaf Lubaszenko", LocalDate.parse("2001-08-24"));

        List<Movie> movies = new ArrayList<>();
        movies.add(movie1);
        movies.add(movie2);
        movies.add(movie3);
        movies.add(movie4);

        SearchEngine engine = new SearchEngine(movies);
        System.out.println(engine.printMovieInfo("Lejdis"));
        System.out.println(engine.printMovieInfo("E=mc2"));



        Book book1 = new Book("Symfonia C++", "Witold Grebosz", LocalDate.parse("2007-03-02"));
        Book book2 = new Book("Czysty Kod", "Robert Martin", LocalDate.parse("2008-02-01"));
        Book book3 = new Book("Czarownik Iwanow", "Gustaw Holubek", LocalDate.parse("2000-02-25"));
        Book book4 = new Book("Achaja 2", "Andrzej Ziemiański", LocalDate.parse("2001-08-24"));

        List<Book> books = new ArrayList<>();
        books.add(book1);
        books.add(book2);
        books.add(book3);
        books.add(book4);

        SearchEngine engBook = new SearchEngine(books);
        System.out.println(engBook.printMovieInfo("Lejdis"));
        System.out.println(engBook.printMovieInfo("E=mc2"));
        books.forEach(System.out::println);

        System.out.println("===================================");
        System.out.println("List sorted by title");
        movies.sort(Comparator.comparing(Movie::getTitle));
        movies.forEach(x-> System.out.println(x.getDescriptionData()));

        System.out.println("===================================");
        System.out.println("List sorted by director and title");
        movies.sort(Comparator.comparing(Movie::getCreator).thenComparing(Movie::getTitle));
        movies.forEach(x-> System.out.println(x.getDescriptionData()));

        System.out.println("===================================");
        System.out.println("Book sorted by title");
        books.sort(Comparator.comparing(Book::getTitle));
        books.forEach(x-> System.out.println(x.getDescriptionData()));

        System.out.println("===================================");
        System.out.println("Book sorted by public date and title");
        books.sort(Comparator.comparing(Book::getPremiereDate).thenComparing(Book::getTitle));
        books.forEach(x-> System.out.println(x.getDescriptionData()));

// porownania za pomoca gotowych klas Comparator i Comparable
        System.out.println("===================================");
        System.out.println("List sorted by Movie.compareTo - czyli metodzie w klasie sort po tytule");
        Collections.sort(movies); // jesli bysmy nie zdefiniowali Movie.compareTo to nie zadziała
        movies.forEach(x-> System.out.println(x.getDescriptionData()));

        System.out.println("---------------------------------------");
        System.out.println("List sorted by MovieByPremiereDate.compare - czyli metodzie w klasie sort po dacie");
        Collections.sort(movies, new MovieByPremiereDate());  //jesli nie podamy drugiego parametru to sortuje domyślnie Movie.compareTo.  W tym przypadku sortuje po MovieByPremiereDate.compare
        movies.forEach(x-> System.out.println(x.getDescriptionData()));

        System.out.println("===================================");
        System.out.println("List sorted by Book.compareTo - czyli metodzie w klasie sort po tytule");
        Collections.sort(books); // jesli bysmy nie zdefiniowali Book.compareTo to nie zadziała
        books.forEach(x-> System.out.println(x.getDescriptionData()));

        System.out.println("---------------------------------------");
        System.out.println("List sorted by BookByPublicDate.compare - czyli metodzie w klasie sort po dacie");
        Collections.sort(books, new BookByPublicDate());  //jesli nie podamy drugiego parametru to sortuje domyślnie Movie.compareTo.  W tym przypadku sortuje po MovieByPremiereDate.compare
        books.forEach(x-> System.out.println(x.getDescriptionData()));


    }
}
