package pl.sda.prog2.w1.movie_search;

import java.time.LocalDate;
import java.util.Comparator;

public class Book extends Creation implements Comparable<Book> {
    private String title;
    private String author;
    private LocalDate publicDate;

    public Book(String title, String author, LocalDate publicDate) {
        this.title = title;
        this.author = author;
        this.publicDate = publicDate;
    }


    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getCreator() {
        return this.author;
    }


    @Override
    public LocalDate getPremiereDate() {
        return publicDate;
    }

    @Override
    public String getDescriptionData() {
        return this.title + " " + this.author + " " + this.publicDate;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", publicDate=" + publicDate +
                '}';
    }

    @Override
    public int compareTo(Book o) {
        return this.title.compareTo(o.getTitle());
    }
}
