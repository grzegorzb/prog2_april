package pl.sda.prog2.w1.movie_search;

import java.util.Comparator;

public class BookByPublicDate implements Comparator<Book> {
    @Override
    public int compare(Book o1, Book o2) {
        return o1.getPremiereDate().compareTo(o2.getPremiereDate());
    }
}
