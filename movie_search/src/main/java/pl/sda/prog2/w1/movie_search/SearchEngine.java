package pl.sda.prog2.w1.movie_search;

import java.util.List;
import java.util.Optional;

public class SearchEngine<E extends Creation> {
    private List<E> creations;

    public SearchEngine(List<E> movies) {
        this.creations = movies;
    }

    private E searchMovie(String title) {
        return creations.stream()
                .filter(c -> c.getTitle().equals(title))
                .findAny()
                .orElse((E) Creation.DEFAULT_CREATION);

    }

    public String printMovieInfo(String title) {
        E creation = searchMovie(title);
//        if("".equals(creation.getTitle())) {
        if(Creation.DEFAULT_CREATION==creation) {  //porownujemy miejsce w pamieci
            return "Movie not found";
        }

        return creation.getTitle() + " " + creation.getCreator() + " " + creation.getPremiereDate();
    }



    private Optional<E> searchMovieOptional(String title) {
        return creations.stream()
                .filter(seek -> seek.getTitle().equals(title))
                .findAny();
    }

    public String printMovieInfoOptional(String title) {
        Optional<E> movie = searchMovieOptional(title);
        if(movie.isPresent()) {
            //movie.get pobiera zawartość optionala
            return movie.get().getTitle() + " " + movie.get().getCreator() + " " + movie.get().getPremiereDate();
        }
        return "Movie not found";
    }


    private E searchMovieException (String title) throws MovieNotFoundException {
        return creations.stream()
                .filter(seek -> seek.getTitle().equals(title))
                .findAny()
                .orElseThrow(MovieNotFoundException::new);
    }

    public String printMovieInfoException(String title) {
        try {
            E creation = searchMovieException(title);
            return creation.getTitle() + " " + creation.getCreator() + " " + creation.getPremiereDate();
        } catch (MovieNotFoundException e) {
            return "Movie not found";
        }

    }








}