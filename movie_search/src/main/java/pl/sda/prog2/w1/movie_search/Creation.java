package pl.sda.prog2.w1.movie_search;
import java.time.LocalDate;

public abstract class Creation {
    public static Creation DEFAULT_CREATION; //wydmuszka, puste miejsce w pamieci dla klasy Creation

    public abstract String getTitle();

    public abstract String getCreator();

    public abstract LocalDate getPremiereDate();

    public abstract String getDescriptionData();


}
