package pl.sda.prog2.w1.movie_search;

public class MovieNotFoundException extends Exception{
        public MovieNotFoundException() {
            super("Movie not found");
        }
}
