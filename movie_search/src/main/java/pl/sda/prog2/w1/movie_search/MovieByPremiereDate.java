package pl.sda.prog2.w1.movie_search;

import java.util.Comparator;

public class MovieByPremiereDate implements Comparator<Movie> {

    @Override
    public int compare(Movie o1, Movie o2) {
        return o1.getPremiereDate().compareTo(o2.getPremiereDate());
    }
}
