package pl.sda.prog2.w1.prog2_25IV.lsp.fix;

import pl.sda.prog2.w1.prog2_25IV.lsp.FileOperation;

public class ReadOnlyFile implements FileWritable, FileReadable {
    @Override
    public byte[] read() {
        return new byte[0];
    }

    @Override
    public void write(byte[] data) {
        throw new UnsupportedOperationException();
    }
}
