package pl.sda.prog2.w1.prog2_25IV;

public class DrivingLicenceRules {
    private static final int DRIVING_LICENCE_REQUIRED_AGE = 18;

    public static boolean canGetDrivingLicence (Person person){
        return person.getAge() > DRIVING_LICENCE_REQUIRED_AGE;
    }

}
