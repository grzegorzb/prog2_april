package pl.sda.prog2.w1.prog2_25IV.lsp.fix;

public interface FileReadable {
    byte[] read();

}
